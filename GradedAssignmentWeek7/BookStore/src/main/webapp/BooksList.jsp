<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>


<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books List</title>
<style>
            .btn {
              background-color: rgb(94, 169, 204);
              border-color: blue;
              color: white;
              padding: 12px 16px;
              font-size: 18px;
              cursor: pointer;
              border-radius: 12px;
              width: 150px;
              margin: 15px;

            }
            
            /* Darker background on mouse-over */
            .btn:hover {
              background-color: rgb(5, 44, 160);
            }
            </style>
</head>
<body>
<a href="index.html"><button class="btn"><b>Home</b></button></a>

</body>
</html>


<%

try {
Class.forName("com.mysql.cj.jdbc.Driver");
} catch (ClassNotFoundException e) {
e.printStackTrace();
}

Connection connection = null;
Statement statement = null;
ResultSet resultSet = null;
%>
<h2 align="center"><font><strong>Books List</strong></font></h2>
<table align="center" cellpadding="5" cellspacing="5" border="1">
<tr>

</tr>
<tr bgcolor="#A52A2A">
<td><b>id</b></td>
<td><b>Books name</b></td>
</tr>
<%
try{ 
connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookstore","root","admin");
statement=connection.createStatement();

resultSet = statement.executeQuery("SELECT * FROM books");
while(resultSet.next()){
%>
<tr bgcolor="#DEB887">

<td><%=resultSet.getString("bookId") %></td>
<td><%=resultSet.getString("bookName") %></td>

</tr>

<% 
}

} catch (Exception e) {
e.printStackTrace();
}
%>
</table>